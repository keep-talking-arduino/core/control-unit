#ifndef MODULEMANAGER_H
#define MODULEMANAGER_H

#include <Arduino.h>
#include <modules/Module.h>
#include <dependencies/DependencyManager.h>
#include <CommandHandler.h>

class ModuleManager {

public:
  ModuleManager init(CommandHandler& commandHandler, DependencyManager& dependencyManager);
  void registerModule(Module module);
  Module* findModule(int address);
  void deleteModule(Module module);
  void clearModules();

  void printReport();

  void generateBomb(int difficulty);
  void startModules();
  void moduleSolved(int address);

private:
  CommandHandler* _commandHandler;
  DependencyManager* _dependencyManager;

  Module _modules[12];
  int findFreeSlot();
};

#endif
