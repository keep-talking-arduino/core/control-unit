#include <modules/ModuleManager.h>

  ModuleManager ModuleManager::init(CommandHandler& commandHandler, DependencyManager& dependencyManager){
    _commandHandler = &commandHandler;
    _dependencyManager = &dependencyManager;
    return *this;
  }

  void ModuleManager::registerModule(Module module){
    int freeSlot = findFreeSlot();

    if(freeSlot >= 0){
      _modules[freeSlot] = module;
    }else{
      Serial.println(F("ModuleManager overflow!"));
    }
  }

  Module* ModuleManager::findModule(int address){
    for(int index = 0; index < 12; index++){
      if(_modules[index].getAddress() == address){
        return &_modules[index];
      }
    }
    return nullptr;
  }

  void ModuleManager::deleteModule(Module module){
    for(int index = 0; index < 12; index++){
      if(_modules[index].equals(module)){
        _dependencyManager->removeDependencies(module);
        _commandHandler->Send(MODULE_CURRENT, _modules[index].getAddress(), TYPE_SET, "STOPMODULE", "");
        _modules[index].dump();
      }
    }
  }
  void ModuleManager::clearModules(){
    _dependencyManager->resetDependencies();
    for(int index = 0; index < 12; index++){
        _commandHandler->Send(MODULE_CURRENT, _modules[index].getAddress(), TYPE_SET, "STOPMODULE", "");
        _modules[index].dump();
    }
  }

  void ModuleManager::printReport(){
      Serial.println(F("----- ModuleManager Status Report -----"));
      for(int index = 0; index < 12; index++){
        if(_modules[index].getAddress() != 0){
          Serial.print(F("Slot: ")); Serial.print(index);
          Serial.print(F(" Name: ")); Serial.print(_modules[index].getName());
          Serial.print(F(" Address: ")); Serial.println(_modules[index].getAddress());
        }
      }
      Serial.println(F("----- End Report -----"));
  }

  void ModuleManager::generateBomb(int difficulty){ // diffeculty ranges from 0% to 100%

  }
  void ModuleManager::startModules(){
    for(int index = 0; index < 12; index++){
      if(_modules[index].getAddress() != 0){
        _commandHandler->Send(MODULE_CURRENT, _modules[index].getAddress(), TYPE_SET, "STARTMODULE", "");
      }
    }
  }
  void ModuleManager::moduleSolved(int address){
    Module* module = findModule(address);
    if(module->getRepetitions() > 0){
      // this.getMo;
    }else{
      deleteModule(*module);
    }
  }

int ModuleManager::findFreeSlot(){
    for(int index = 0; index < 12; index++){
      if(_modules[index].getAddress() == 0){
        return index;
      }
    }
    return -1;
}
