#include <modules/Module.h>

  Module::Module(){
    _name = "NULL";
    _address = 0;
  }

  Module::Module(String name, int address){
    _name = name;
    _address = address;
  }

  Module Module::dump(){
    _name = "NULL";
    _address = 0;
    _repetitions = 0;
    return *this;
  }

  String Module::getName(){
    return _name;
  }
  Module Module::setName(String name){
    _name = name;
    return *this;
  }

  int Module::getAddress(){
    return _address;
  }
  Module Module::setAddress(int address){
    _address = address;
    return *this;
  }

  int Module::getRepetitions(){
    return _repetitions;
  }
  Module Module::setRepetitions(int repetitions){
    _repetitions = repetitions;
    return *this;
  }

  bool Module::equals(Module module){
    if(module.getAddress() == _address && module.getName().equals(_name) && module.getRepetitions() == _repetitions){
      return true;
    }
    return false;
  }
