#ifndef MODULE_H
#define MODULE_H

#include <Arduino.h>

class Module {

public:
  Module();
  Module(String name, int Address);
  Module dump();
  String getName();
  Module setName(String name);

  int getAddress();
  Module setAddress(int address);

  int getRepetitions();
  Module setRepetitions(int repetitions);

  bool equals(Module module);


private:
  String _name;
  int _address;

  int _repetitions = 0;

};

#endif
