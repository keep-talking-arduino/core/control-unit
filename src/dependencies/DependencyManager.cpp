#include <dependencies/DependencyManager.h>

DependencyManager DependencyManager::init(CommandHandler& commandHandler){

  _commandHandler = &commandHandler;
  resetDependencies();

  return *this;
}

DependencyManager DependencyManager::registerDependency(Module& module, String property){


  if(property.equals("SERIAL")){
    if(!contains(_serialDependent, module)){
      int freeSlot = findFreeSlot(_serialDependent);
      _serialDependent[freeSlot] = &module;
    }
  }else if(property.equals("MAXSTRIKES")){
    if(!contains(_maxStrikesDependent, module)){
      int freeSlot = findFreeSlot(_maxStrikesDependent);
      _maxStrikesDependent[freeSlot] = &module;
    }
  }else if(property.equals("BATTERIES")){
    if(!contains(_batteriesDependent, module)){
      int freeSlot = findFreeSlot(_batteriesDependent);
      _batteriesDependent[freeSlot] = &module;
    }
  }else if(property.equals("CARINDICATOR")){
    if(!contains(_carIndicatorDependent, module)){
      int freeSlot = findFreeSlot(_carIndicatorDependent);
      _carIndicatorDependent[freeSlot] = &module;
    }
  }else if(property.equals("FRKINDICATOR")){
    if(!contains(_frkIndicatorDependent, module)){
      int freeSlot = findFreeSlot(_frkIndicatorDependent);
      _frkIndicatorDependent[freeSlot] = &module;
    }
  }else if(property.equals("CLRINDICATOR")){
    if(!contains(_clrIndicatorDependent, module)){
      int freeSlot = findFreeSlot(_clrIndicatorDependent);
      _clrIndicatorDependent[freeSlot] = &module;
    }
  }

  return *this;
}


DependencyManager DependencyManager::removeDependency(Module& module, String property){

  if(property.equals("SERIAL")){
    removeDependency(_serialDependent, module);
  }else if(property.equals("MAXSTRIKES")){
    removeDependency(_maxStrikesDependent, module);
  }else if(property.equals("BATTERIES")){
    removeDependency(_batteriesDependent, module);
  }else if(property.equals("CARINDICATOR")){
    removeDependency(_carIndicatorDependent, module);
  }else if(property.equals("FRKINDICATOR")){
    removeDependency(_frkIndicatorDependent, module);
  }else if(property.equals("CLRINDICATOR")){
    removeDependency(_clrIndicatorDependent, module);
  }

  return *this;
}

DependencyManager DependencyManager::removeDependencies(Module& module){
    removeDependency(_serialDependent, module);
    removeDependency(_maxStrikesDependent, module);
    removeDependency(_batteriesDependent, module);
    removeDependency(_carIndicatorDependent, module);
    removeDependency(_frkIndicatorDependent, module);
    removeDependency(_clrIndicatorDependent, module);

  return *this;
}

DependencyManager DependencyManager::resetDependencies(){

  _serialDependent[12] = {};
  _strikesDependent[12] = {};
  _maxStrikesDependent[12] = {};
  _batteriesDependent[12] = {};
  _carIndicatorDependent[12] = {};
  _frkIndicatorDependent[12] = {};
  _clrIndicatorDependent[12] = {};

  return *this;
}

DependencyManager DependencyManager::printReport(){

  Serial.println(F("----- Dependency Status Report -----"));

  Serial.print(F("Serial Dependent: "));
  printDependency(_serialDependent); Serial.println();
  Serial.print(F("Strikes Dependent: "));
  printDependency(_strikesDependent); Serial.println();
  Serial.print(F("Max Strikes Dependent: "));
  printDependency(_maxStrikesDependent); Serial.println();
  Serial.print(F("Batteries Dependent: "));
  printDependency(_batteriesDependent); Serial.println();
  Serial.print(F("Car Indicator Dependent: "));
  printDependency(_carIndicatorDependent); Serial.println();
  Serial.print(F("Frk Indicator Dependent: "));
  printDependency(_frkIndicatorDependent); Serial.println();
  Serial.print(F("Clr Indicator Dependent: "));
  printDependency(_clrIndicatorDependent); Serial.println();

  Serial.println(F("----- End Report -----"));

  return *this;
}

DependencyManager DependencyManager::printDependency(Module* dependencyArray[12]){
  for(int i = 0; i < 12; i++){
    if(dependencyArray[i] != NULL){
      Serial.print(dependencyArray[i]->getName());
      Serial.print(", ");
    }
  }
  return *this;
}

String DependencyManager::getSerial(){
  return _serial;
}
DependencyManager DependencyManager::setSerial(String serial){
  _serial = serial;
  informDependents(_serialDependent, "SETSERIAL", _serial);
  return *this;
}

int DependencyManager::getStrikes(){
  return _strikes;
}
DependencyManager DependencyManager::setStrikes(int strikes){
  _strikes = strikes;
  // prevents updating when bomb has exploded
  if(_strikes <= _maxStrikes){
    //make clock tick faster
    int clockSpeed = 1000;
    switch (_strikes) {
      case 1: clockSpeed = 850;
      case 2: clockSpeed = 650;
    }
    _commandHandler->Send(MODULE_CURRENT, MODULE_CLOCK, TYPE_SET, "SETSPEED", String(clockSpeed));
    informDependents(_strikesDependent, "SETSTRIKES", String(_strikes));
  }
  return *this;
}
DependencyManager DependencyManager::addStrike(){
  setStrikes(getStrikes()+1);
  return *this;
}

int DependencyManager::getMaxStrikes(){
  return _maxStrikes;
}
DependencyManager DependencyManager::setMaxStrikes(int maxStrikes){
  _maxStrikes = maxStrikes;
  informDependents(_maxStrikesDependent, "SETMAXSTRIKES", String(_maxStrikes));
  return *this;
}

int DependencyManager::getBatteries(){
  return _batteries;
}
DependencyManager DependencyManager::setBatteries(int batteries){
  _batteries = batteries;
  informDependents(_batteriesDependent, "SETBATTERIES", String(_batteries));
  return *this;
}

bool DependencyManager::getCarIndicator(){
  return _carIndicator;
}
DependencyManager DependencyManager::setCarIndicator(bool carIndicator){
  _carIndicator = carIndicator;
  informDependents(_carIndicatorDependent, "SETCAR", String(_carIndicator));
  return *this;
}

bool DependencyManager::getFrkIndicator(){
  return _frkIndicator;
}
DependencyManager DependencyManager::setFrkIndicator(bool frkIndicator){
  _frkIndicator = frkIndicator;
  informDependents(_frkIndicatorDependent, "SETFRK", String(_frkIndicator));
  return *this;
}

bool DependencyManager::getClrIndicator(){
  return _clrIndicator;
}
DependencyManager DependencyManager::setClrIndicator(bool clrIndicator){
  _clrIndicator = clrIndicator;
  informDependents(_clrIndicatorDependent, "SETCLR", String(_clrIndicator));
  return *this;
}

int DependencyManager::findFreeSlot(Module* dependencyArray[12]){
  for(int index = 0; index < 12; index ++){
    if(dependencyArray[index] == NULL){
      return index;
    }
  }
  return -1;
}

void DependencyManager::informDependents(Module* dependencyArray[12], String command, String value){
  for(int index = 0; index < 12; index++){
    if(dependencyArray[index] != NULL){
      _commandHandler->Send(MODULE_CURRENT, dependencyArray[index]->getAddress(), TYPE_SET, command, String(value));
    }
  }
}

void DependencyManager::removeDependency(Module* dependencyArray[12], Module& module){
  for(int index = 0; index < 12; index++){
    if(dependencyArray[index]->equals(module)){
      dependencyArray[index] = NULL;
    }
  }
}

bool DependencyManager::contains(Module* dependencyArray[12], Module& module){
  for(int index = 0; index < 12; index++){
    if(dependencyArray[index]->equals(module)){
      return true;
    }
  }
  return false;
}
