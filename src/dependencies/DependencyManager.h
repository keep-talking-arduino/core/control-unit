#ifndef DEPENDENCYMANAGER_H
#define DEPENDENCYMANAGER_H

#include <Arduino.h>
#include <CommandHandler.h>
#include <modules/Module.h>


class DependencyManager {

public:
  DependencyManager init(CommandHandler& commandHandler);
  DependencyManager registerDependency(Module& module, String property);
  DependencyManager removeDependency(Module& module, String property);
  DependencyManager removeDependencies(Module& module);
  DependencyManager resetDependencies();
  DependencyManager printReport();
  DependencyManager printDependency(Module* dependencyArray[12]);

  String getSerial();
  DependencyManager setSerial(String serial);

  int getStrikes();
  DependencyManager setStrikes(int strikes);
  DependencyManager addStrike();

  int getMaxStrikes();
  DependencyManager setMaxStrikes(int maxStrikes);

  int getBatteries();
  DependencyManager setBatteries(int batteries);

  bool getCarIndicator();
  DependencyManager setCarIndicator(bool carIndicator);

  bool getFrkIndicator();
  DependencyManager setFrkIndicator(bool frkIndicator);

  bool getClrIndicator();
  DependencyManager setClrIndicator(bool clrIndicator);

private:
  CommandHandler* _commandHandler;

  String _serial;
  int _strikes;
  int _maxStrikes;
  int _batteries;
  bool _carIndicator;
  bool _frkIndicator;
  bool _clrIndicator;

  Module* _serialDependent[12];
  Module* _strikesDependent[12];
  Module* _maxStrikesDependent[12];
  Module* _batteriesDependent[12];
  Module* _carIndicatorDependent[12];
  Module* _frkIndicatorDependent[12];
  Module* _clrIndicatorDependent[12];

  int findFreeSlot(Module* dependencyArray[12]);
  void informDependents(Module* dependencyArray[12], String command, String value);
  void removeDependency(Module* dependencyArray[12], Module& module);
  bool contains(Module* dependencyArray[12], Module& module);
};

#endif
