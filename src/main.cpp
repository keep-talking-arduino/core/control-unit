#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <CommandHandler.h>
#include <ShiftHandler.h>
#include <dependencies/DependencyManager.h>
#include <modules/ModuleManager.h>
#include <MemoryFree.h>

ShiftHandler shiftHandler;
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
CommandHandler cmdHandler;
ModuleManager moduleManager;
DependencyManager dependencyManager;

void shiftStrikes(int strikes){
  shiftHandler.clear();
  if(dependencyManager.getStrikes() == 2){
    if(strikes == 0){
      shiftHandler.setValue(0, 1, true);
      shiftHandler.setValue(0, 2, true);
      shiftHandler.setValue(0, 4, true);
      shiftHandler.setValue(0, 5, true);
    }
    if(strikes == 1){
      shiftHandler.setValue(0, 1, true);
      shiftHandler.setValue(0, 2, true);
    }
    shiftHandler.shift();
  }
}

void explode(){
  cmdHandler.Send(MODULE_CURRENT, MODULE_CLOCK, TYPE_SET, F("STOPTIME"), "");

  // disable all registered modules
  moduleManager.clearModules();

  // print report
  Serial.println(F("Bomb Exploded!!!"));
  Serial.print(F("Time left: "));
  Serial.println(F("X"));
  Serial.print(F("Modules disarmed: "));
  Serial.println(F("X"));
  Serial.print(F("Modules left: "));
  Serial.println(F("X"));
}

void addStrike(){
  dependencyManager.addStrike();

  // explode when surpassing max strike count
  if(dependencyManager.getStrikes() > dependencyManager.getMaxStrikes()){
    explode();
  }
  shiftStrikes(dependencyManager.getStrikes());
}

void ProcessCommand(Command cmd){
  Serial.println(cmd.ToString());

  if(cmd.GetMessageType().equals(TYPE_REGISTER)){
    if(cmd.GetAction().equals(F("MODULE"))){
      Module module = Module(cmd.GetValue(), cmd.GetSender());
      moduleManager.registerModule(module);

      cmdHandler.Send(MODULE_CURRENT, cmd.GetSender(), TYPE_RESPONSE, F("REGISTER"), F("OK"));
    }else if(cmd.GetAction().equals(F("DEPENDENCY"))){
      Module* module = moduleManager.findModule(cmd.GetSender());
      dependencyManager.registerDependency(*module, cmd.GetValue());
    }
  }
  else if(cmd.GetMessageType().equals(TYPE_SET)){
    if(cmd.GetAction().equals(F("ADDSTRIKE"))){
      addStrike();
    }
  }
  else if(cmd.GetMessageType().equals(TYPE_NOTIFY)){
    if(cmd.GetAction().equals(F("EXPLODE"))){
      explode();
    }
    if(cmd.GetAction().equals(F("SOLVED"))){
      moduleManager.moduleSolved(cmd.GetSender());
    }
  }
}

void setup(){
  Serial.begin(57600);
  Serial.print(F("Free Memory: "));Serial.print(freeMemory()); Serial.print(" bytes (");Serial.print((float)freeMemory()/2048*100); Serial.println("%)");
  Serial.println(F("CONTROL UNIT"));
  randomSeed(analogRead(1));
  cmdHandler.Begin(MODULE_CTRLUNIT, ProcessCommand);
  dependencyManager.init(cmdHandler);
  moduleManager.init(cmdHandler, dependencyManager);

  // clock //latch //data
  shiftHandler.init(11, 12, 10);

  // initiate LCD display
  lcd.begin(16,2);
  lcd.clear();
  lcd.setCursor(4,0);
  lcd.print(F("Serial #"));
  lcd.setCursor(4,1);

  // generate, store and display Serial Number
  String letters = F("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
  String serial = F("");
  for(int i = 0; i < 5; i++){
    serial.concat(letters.charAt(random(0, 36)));
  }
  serial.concat(random(0, 10)); // must end on digit
  dependencyManager.setSerial(serial);
  lcd.print(dependencyManager.getSerial());

    delay(5000);

  // generate bomb
  moduleManager.generateBomb(20);

  // output strikes
  shiftStrikes(dependencyManager.getStrikes());

  // clock
  cmdHandler.Send(MODULE_CURRENT, MODULE_CLOCK, TYPE_SET, F("SETTIME"), "5,00");

  // start generated modules
  moduleManager.startModules();
  cmdHandler.Send(MODULE_CURRENT, MODULE_CLOCK, TYPE_SET, F("STARTTIME"), "");

}
void loop(){
  cmdHandler.Run();
}
